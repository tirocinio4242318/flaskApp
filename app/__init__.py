from flask import Flask
import pymongo

client = pymongo.MongoClient('localhost', 27017)
db = client.user_login_system


app = Flask(__name__)
app.secret_key = "abcdefg"

from app import views