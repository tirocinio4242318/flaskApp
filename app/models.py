from flask import Flask, jsonify, request, redirect, session, flash
from app import app
from app import db

import uuid


class User:
    #Crea una sessione
    def start_session(self, user):
        del user['password']
        session['loggedIn'] = True
        session['user'] = user
        return jsonify(user), 200

    #Effettua la registrazione
    def signup(self):
        #Acqusizione dati dal form
        req = request.form
        user = {
            "_id": uuid.uuid4().hex,
            "username": req["username"],
            "email": req["email"],
            "password": req["password"]
        }

        #Inserimento del nuovo utente nel db
        if db.users.find_one({"email": user['email']}):
            flash('Email attualmente in uso. Se possiedi un account effettua il login')
            return redirect("/sign-up")
        else:
            db.users.insert_one(user)
            self.start_session(user)
            return redirect("/dashboard")

    #Effettua il logout
    def signout(self):
        #Elimina la sessione
        session.clear()
        return redirect("/")

    #Effettua il login
    def login(self):
        #Se le credenziali dell'utente sono presenti nel db crea la sessione
        user = db.users.find_one({"username": request.form.get('username')})
        if user and request.form.get('password') == user['password']:
            self.start_session(user)
            return redirect("/dashboard")
        else:
            flash('Credenziali errate. Riprova.')
            return redirect("/login")

