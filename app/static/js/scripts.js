document.addEventListener('DOMContentLoaded', function() {
    var chart1Button = document.getElementById('chart1Button');
    var chart2Button = document.getElementById('chart2Button');
    var currentChart = 'chart1';

    chart1Button.addEventListener('click', function() {
        changeChart('chart1');

    });

    chart2Button.addEventListener('click', function() {
        changeChart('chart2');
    });

    function generateChart(chart, chartData) {
        // Genera il grafico utilizzando i dati
        Plotly.plot(chart, chartData, {});
    }

    function changeChart(chart) {
        // Nascondi il grafico corrente
        var currentChartElement = document.getElementById(currentChart);
        currentChartElement.style.display = 'none';

        // Mostra il nuovo grafico
        var newChartElement = document.getElementById(chart);
        newChartElement.style.display = 'block';

        // Aggiorna lo stato corrente del grafico
        currentChart = chart;
    }

    // Recupera i dati del grafico 1 dalla variabile globale passata dal server
    var chart1Data = JSON.parse(document.getElementById('chart1').dataset.plot);

    // Genera il grafico 1
    generateChart('chart1', chart1Data);

    // Recupera i dati del grafico 2 dalla variabile globale passata dal server
    var chart2Data = JSON.parse(document.getElementById('chart2').dataset.plot);

    // Genera il grafico 2
    generateChart('chart2', chart2Data);
});