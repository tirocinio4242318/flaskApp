import json
from app import app,db
from flask import render_template, request, session, redirect, jsonify
from functools import wraps
from app.models import User
import pandas as pd
import plotly
import plotly.express as px



#Decoratore per impedire all'utente di accedere alla dashboard senza accedere
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'loggedIn' in session:
            return f(*args, *kwargs)
        else:
            return redirect("/")

    return wrap

#Route index
@app.route('/')
def index():

    #Query db
    list_data = db.prova.find()

    #Elaborazione con Pandas
    df = pd.DataFrame(list_data)

    #Creazione dei grafici
    fig1 = px.line(df, x='giorno', y='misurazione', title='Grafico a linea')
    fig2 = px.bar(df, x='giorno', y='misurazione', title='Grafico a barre')

    #Conversione dei grafici in JSON
    graphJSON1 = json.dumps(fig1, cls=plotly.utils.PlotlyJSONEncoder)
    graphJSON2 = json.dumps(fig2, cls=plotly.utils.PlotlyJSONEncoder)

    return render_template("public/index.html", plot1=graphJSON1, plot2=graphJSON2)

#Route about
@app.route('/about')
def about():
    return render_template("public/about.html")

#Route dashboard
@app.route('/dashboard')
@login_required
def dashboard():
    return render_template("public/dashboard.html")

#Route registrazione
@app.route('/sign-up', methods=["GET","POST"])
def sign_up():
    #Chiamata al metodo registrazione della classe User
    if request.method == "POST":
        return User().signup()
    return render_template("/public/sign_up.html")

#Route sign-out
@app.route('/sign-out', methods=["GET","POST"])
def sign_out():
    #Chiamata al metodo signout della classe User
    return User().signout()

#Route login
@app.route('/login',methods=["GET", "POST"])
def login():
    #Chiamata al metodo login della classe User
    if request.method == "POST":
        return User().login()
    return render_template("/public/login.html")




